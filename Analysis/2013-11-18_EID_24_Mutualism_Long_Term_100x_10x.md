post_id: 47
title: Experiment 24: Comparing 10x / 100x dilution factor
status: publish
categories: Population Dynamics
tags: experiment, mutualism, dh5a-pBbS5cRFP, dh5a-pSB6A1Jx16YFP

# Questions/Goals

Check what happens when using 10x / 100x dilution factors.

# Results

Results are available [here]([nbview][bit]Analysis/2013-11-18_EID_24_Mutualism_Long_Term_100x_10x.ipynb[/bit][/nbview])

