post_id: 49
title: Experiment 23: multiple day experiment to check mutualism
status: publish
categories: Population Dynamics
tags: experiment, mutualism, dh5a-pBbS5cRFP, dh5a-pSB6A1Jx16YFP

# Questions/Goals

Confirm the following two features:

1. The mutualism. Both strains can really grow in conditions where they cannot grow alone. [Answer: Yes]
    * Specifically, run in parallel an experiment where each strains is growing on its own.
2. Confirm that the new strains are oscillating. [Answer: Yes.]
    * Bulk fluorescence is oscillating (see below)
    * Single cell fluorescence is oscillating (not in this analysis)

## Findings from Plate Reader Data

1. Chloramphenicol = 10-12 ug/ml. Fluctuations in the total population size right before the mutualism fails?
    * Could the population oscillate into extinction?

2. The mutualism can be transient.
    *  At high Ampicillin ([amp] = 200 ug/ml), depending on the chloramphenicol concentrations, the strains sometimes survive for a few days before the population goes extinct.
    
3. Oscillations are present both in the "mutualistic region" as well as outside of it.
    * Worth highlighting, even if it may be obvious to us.
    
4. No evidence of a significant interaction between chloramphenciol and ampicillin.
    * For a single strain, at the range of concentrations we're probing, growth depends only on a single antibiotic concentration (not on both).

## Findings from Flow Cytometry Data

1. Strength of oscillations may not be monotonic in antibiotic concentrations.
2. "Period" 3 oscillations across a large region of parameter space. Also, many of these time traces look very similar.
3. Regions (low ampicillin concentration) where we have "spikes".
    
## Questions

1. If you look at the ampicillin resistant strain when it's growing on its own in 2 ug/ml of chloramphenicol, the total population size increases with time. 
    * What is going on?

## Experiment set up

* Grew chlR, ampR in 3 mL LB cultures for 24 hours in the presence of proper selection (ampicillin or chloramphenicol).
* On the following day, started 3x 3 ml LB cultures (all with NO selection):
    * 1x ChlR 3 ml culture (100x dilution from previous day)
    * 1x AmpR 3 ml culture (100x dilution from previous day)
    * 1x 1:1 ChlR:AmpR 3 ml culture (100x dilution from previous day).
    * (The initial cell density of all cultures should have been the same.)
* On the following day (Day 0) (~ 4 hours of work):
    * Prepared 3x plates with ampicillin along the rows and chloramphenicol along the columns.
    * Inoculated 3 mL cultures into plates at a 100x dilution.
* On each following day (Day 1-9/10) (~2 hours of work / day):
    * Prepared 3x plates with ampicillin / chloramphenicol.
    * Propagated from previous plates at a 100x dilution.
    
* Experimented got terminated prematurely due to failed shaker (on day 10).   

# Analysis

Analysis are available [here]([nbview][bit]ProjectBlackDove/Progress/Analysis/2013-10-October/2013-10-21_EID_23_MutualismLongTerm/2013-10-21_EID_23_Mutualism_Long_Term.ipynb[/bit][/nbview])

