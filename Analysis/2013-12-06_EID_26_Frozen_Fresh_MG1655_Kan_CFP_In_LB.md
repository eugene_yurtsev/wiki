post_id: 13
title: Experiment 26
status: publish
categories: calibration
tags: experiment, bacteria, growth curve

# Questions/Goals

Check whether plates can be prepared in batch then frozen and thawed right before the experiment.

# Experimental details

- Plate frozen at -20C with 180 uL LB on rows A-D.
- Plate thawed 4 hours prior to experiment.
- 180 uL LB added to rows E-H after thawing.

- 20 uL of cells were added to each well to make:
    Rows A, H: dilution factor of 10x
    Rows B, G: dilution factor of 10^3x
    Rows C, F: dilution factor of 10^5x
    Rows D, E: dilution factor of 10^7x

- Plate was placed in the plate reader at 6:20 PM. But unfortunately the measurement did not start until 7:20 PM. So the plate was
not being shaken or measured for 1 hour. However, the plate was at 37C degrees during that one hour (as well as during the growth).

# Results

Figures are available [here]([nbview][bit]Analysis/2013-12-06_EID_26_Frozen_Fresh_MG1655_Kan_CFP_In_LB.ipynb[/bit][/nbview])

# Conclusions

- There are some slight differences between fresh and frozen paltes, but they do not seem to be important.
