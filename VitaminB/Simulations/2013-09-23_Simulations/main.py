#!/usr/bin/env python
from VitaminB.models import model_2_analytic as model
from VitaminB.simulation import GridSimulator, simulate_bifurcation
from VitaminB import simulation
from pylab import *

def run_sim(fname):
    par_dict = simulation.get_par_dict()

    par_dict.update({
        'cycles' : 20,
        'N1_single' : 1.0,
        'N1_connected' : r_[ones(10), zeros(10)],
        'Vmax' : 100.0
        })

    par_dict['m'] = 0.25
    par_dict['cycle_duration'] = linspace(3, 18, 20)
    #par_dict['cycle_duration'] = linspace(3, 18, 2)
    par_dict['Ai'] = r_[0, logspace(0, 2, 20)[1:]]
    #par_dict['Ai'] = r_[0, logspace(0, 2, 2)[1:]]
    par_dict['dilution_factor'] = logspace(0, 4, 100.0)
    #par_dict['dilution_factor'] = logspace(0, 4, 10.0)

    par_dict['model'] = model
    par_dict['Nmin'] = 10**-6
    par_dict['tlag'] = 0
    par_dict['m'] = 0.25

    a = simulation.GridSimulator(simulate_bifurcation, loop_pars=['Ai', 'cycle_duration'], **par_dict)
    a.run()
    a.extract(start_index_for_fitting=5)
    a.save(fname)

def plot_velocity(sim, result, row, col):
    result.plot_single_pop_density(annotate=False, marker='o', label='Density')
    xscale('log')
    result.plot_velocity(shareax=gca(), label='Velocity')
    xlim(1, 10000)
    result.plot_pin_locations(color='r')
    xlim(1, 10000)
    yticks([])
    #legend(loc='best')

def main():
    fname = './Simulations/sim_02.pkl'
    run_sim(fname)

    #z = GridSimulator.load(fname)
    #z.extract(start_index_for_fitting=0)
    #z.grid_plot(plot_velocity, row_label_xoffset=0.5, col_label_yoffset=0.5)
    #plt.show()

if __name__ == '__main__':
    main()
